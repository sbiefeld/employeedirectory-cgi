﻿# Employee Directory

## Business Need:
* Create a web application that displays Employee information when employee id is
entered on the page (mockup A1)

* When the employee cannot be found let the user know it cannot be found

* Create new employees when the user clicks ADD new employees (mockup A2)

* Create a Restful Web Service that retrieves, and inserts data to and from the data base

* Create an Endpoint that get employee information

* Create an Endpoint that create new employee

* Us the provided SQL script to create the database to that the application will use.(mockup A3)

##Tooling Requirements

* sqlexpress
* VS 2015
* [Git](http://git-scm.com/)
* [Node.js](http://nodejs.org/) (with npm)
* [nwb](https://github.com/insin/nwb/) - `npm install -g nwb`

## Building the application
1. clone the repo
2. run `.\fake ci` in powershell for first time install
3. open solution in visual studio
4. run the API
5. run the web app
6. open powershell to `cd EmployeeDirectory\src\EmployeeDirectory.Web\Assets\js\react-app`
7. run `nwb serve`
8. go back to visual studio and run `EmployeeDirectory.Web`

## Server Stack
#### Database
  * Entity Framework 6
  * DB Migrations via RoundHouse
#### Web
  * ASP.NET MVC 5
  * WebAPI 2
  * Feature Folders
  * MediatR
  * Automapper
#### Dependency Injection
  * Structuremap
#### Testing
  * Fixie
  * Autofixture

## UI Stack
  * React views
  * Skeleton CSS
  * WebPack
  * ES 6/Babel


#### Building UI

You do not need to run these locally; the build script will run it as part of the CI

* `nwb build` (production)
* `nwb build --set-env-NODE_ENV=development` (development)