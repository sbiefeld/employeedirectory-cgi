﻿namespace EmployeeDirectory.Core.Domain
{
    using System;

    public class Address : Entity
    {
        public string StreetAddress { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public AddressType AddressType { get; set; }
        public Guid EmployeeId { get; set; }
        public Employee Employee { get; set; }
    }
}