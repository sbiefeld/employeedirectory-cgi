﻿namespace EmployeeDirectory.Core.Domain
{
    public class AddressType : Entity
    {
        public string AddressTypeDisplayName { get; set; }
    }
}