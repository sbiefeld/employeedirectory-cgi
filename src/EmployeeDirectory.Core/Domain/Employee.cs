﻿namespace EmployeeDirectory.Core.Domain
{
    using System;
    using System.Collections.Generic;

    public class Employee : Entity
    {
        public Employee()
        {
            Addresses = new List<Address>();
        }

        public string EmployeeDisplayId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime StartDate { get; set; }
        public bool Active { get; set; }
        public List<Address> Addresses { get; set; }
    }
}
