﻿namespace EmployeeDirectory.Core.Extensions
{
    using System;

    public static class StringExtensions
    {
        public static bool MatchesDiscountRule(this string name, string value)
        {
            if (string.IsNullOrEmpty(name))
            {
                return false;
            }
            return name.Trim().StartsWith(value.Trim(), StringComparison.InvariantCultureIgnoreCase);
        }
    }
}
