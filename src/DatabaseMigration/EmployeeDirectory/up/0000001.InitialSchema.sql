﻿-- drop table Address
-- drop table Employee
-- drop table AddressType
-- go

create table Employee (
Id uniqueidentifier not null CONSTRAINT [DF_Employee_Id] DEFAULT NEWSEQUENTIALID() CONSTRAINT [PK_Employee] PRIMARY KEY,
EmployeeDisplayId varchar(10) not null,
FirstName varchar(30) not null,
LastName varchar(30) not null,
StartDate date not null,
Active bit not null)
 
create table Address (
Id uniqueidentifier not null CONSTRAINT [DF_Address_Id] DEFAULT NEWSEQUENTIALID() CONSTRAINT [PK_Address] PRIMARY KEY,
EmployeeId uniqueidentifier not null,
AddressTypeId uniqueidentifier not null,
StreetAddress varchar(60) null,
City varchar(30) null,
State varchar(2) null,
Zip varchar(5) null,
Active bit not null)
 
create table AddressType (
Id uniqueidentifier not null CONSTRAINT [DF_AddressType_Id] DEFAULT NEWSEQUENTIALID() CONSTRAINT [PK_AddressType] PRIMARY KEY,
AddressTypeDisplayName varchar(30) not null,
Active bit not null)

alter table Employee
add constraint UQ_employee_employee_display_id unique (EmployeeDisplayId)

alter table Address
add constraint FK_address_address_type foreign key (AddressTypeId)
    references AddressType(Id)

alter table Address
add constraint FK_address_employee foreign key (EmployeeId)
    references Employee(Id)
 
go 
 
 
insert into AddressType (Id, AddressTypeDisplayName, Active)
values ('6D957F09-B8D0-4353-8340-7A6256B181F1', 'Home', 1)
 
insert into AddressType (Id, AddressTypeDisplayName, Active)
values ('74451A49-2D19-46A1-8A99-9F8A7C8BB4B1', 'Mailing', 1)
 
insert into Employee (Id, EmployeeDisplayId, FirstName, LastName, StartDate, Active)
values ('6BD6C07B-7A1B-4DD4-AD59-5884AAC9C24D', '1', 'Andy', 'French', getdate(), 1)
 
insert into Employee (Id, EmployeeDisplayId, FirstName, LastName, StartDate, Active)
values ('47126091-2889-4FA8-BCF3-F42F75A08045', '2', 'Kyle', 'Smith', getdate(), 1)

insert into Address (Id, EmployeeId, AddressTypeId, StreetAddress, City, State, Zip, Active)
values (newid(), '6BD6C07B-7A1B-4DD4-AD59-5884AAC9C24D', '6D957F09-B8D0-4353-8340-7A6256B181F1', '5975 Andy St.', 'San Diego', 'CA', '92130', 1)
 
insert into Address (Id, EmployeeId, AddressTypeId, StreetAddress, City, State, Zip, Active)
values (newid(), '6BD6C07B-7A1B-4DD4-AD59-5884AAC9C24D', '74451A49-2D19-46A1-8A99-9F8A7C8BB4B1', '9991 Andy Mailing St.', 'San Diego', 'CA', '92130', 1)
 
insert into Address (Id, EmployeeId, AddressTypeId, StreetAddress, City, State, Zip, Active)
values (newid(), '47126091-2889-4FA8-BCF3-F42F75A08045', '6D957F09-B8D0-4353-8340-7A6256B181F1', '1234 Kyle Rd..', 'San Diego', 'CA', '92130', 1)
 
insert into Address (Id, EmployeeId, AddressTypeId, StreetAddress, City, State, Zip, Active)
values (newid(), '47126091-2889-4FA8-BCF3-F42F75A08045', '74451A49-2D19-46A1-8A99-9F8A7C8BB4B1', '8888 Kyle Mailing Rd.', 'San Diego', 'CA', '92130', 1)