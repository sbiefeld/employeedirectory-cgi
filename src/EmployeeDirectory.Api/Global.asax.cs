﻿namespace EmployeeDirectory.Api
{
    using System.Linq;
    using System.Web;
    using System.Web.Http;
    using System.Web.Mvc;
    using FluentValidation.WebApi;
    using Infrastructure;
    using Infrastructure.Validation;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Serialization;

    public class WebApiApplication : HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            ConfigureJsonSerialization();
            EnableFeatureFolders();
            InitializeFluentValidation();
            FilterConfig.RegisterGlobalFilters(GlobalConfiguration.Configuration.Filters);
        }

        private void ConfigureJsonSerialization()
        {
            GlobalConfiguration.Configuration
              .Formatters
              .JsonFormatter
              .SerializerSettings
              .ContractResolver = new CamelCasePropertyNamesContractResolver();

            GlobalConfiguration.Configuration
                .Formatters
                .JsonFormatter
                .SerializerSettings
                .NullValueHandling = NullValueHandling.Ignore;
        }

        private static void EnableFeatureFolders()
        {
            ControllerBuilder.Current.SetControllerFactory(new FeatureFolderControllerFactory());
        }

        private static void InitializeFluentValidation()
        {
            FluentValidationModelValidatorProvider.Configure(GlobalConfiguration.Configuration, provider => provider.ValidatorFactory = new FluentValidatorFactory());
        }
    }
}
