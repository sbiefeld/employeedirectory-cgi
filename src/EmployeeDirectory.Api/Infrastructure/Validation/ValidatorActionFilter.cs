﻿namespace EmployeeDirectory.Api.Infrastructure.Validation
{
    using System.Net;
    using System.Net.Http;
    using System.Text;
    using System.Web.Http.Controllers;
    using System.Web.Http.Filters;
    using System.Web.Http.ModelBinding;
    using System.Web.Http.Results;
    using Newtonsoft.Json;

    public class ValidatorActionFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (!actionContext.ModelState.IsValid)
            {
                var result = new JsonResult<ModelStateDictionary>(actionContext.ModelState, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                }, Encoding.UTF8, actionContext.Request);

                var response = actionContext.Request.CreateResponse(
                HttpStatusCode.BadRequest, result);

                response.Headers.Add("Access-Control-Allow-Headers", new[] { "content-type", "x-requested-with" });
                response.Headers.Add("Access-Control-Allow-Origin", "*");

                actionContext.Response = response;
            }
        }
    }
}