﻿namespace EmployeeDirectory.Api.Infrastructure
{
    using System.Web.Http;
    using System.Web.Http.Controllers;
    using System.Web.Http.Filters;
    using Core.Infrastructure;
    using ActionFilterAttribute = System.Web.Http.Filters.ActionFilterAttribute;

    public class UnitOfWork : ActionFilterAttribute
    {public override void OnActionExecuting(HttpActionContext filterContext)
        {
            var database = (EmployeeDirectoryContext)GlobalConfiguration.Configuration.DependencyResolver.GetService(typeof(EmployeeDirectoryContext));

            database.BeginTransaction();
        }

        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            var database = (EmployeeDirectoryContext)GlobalConfiguration.Configuration.DependencyResolver.GetService(typeof(EmployeeDirectoryContext));

            database.CloseTransaction(actionExecutedContext.Exception);
        }
    }
}