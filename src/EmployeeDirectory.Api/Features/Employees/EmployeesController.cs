﻿namespace EmployeeDirectory.Api.Features.Employees
{
    using System.Threading.Tasks;
    using System.Web.Http;
    using System.Web.Http.Cors;
    using MediatR;

    [AllowAnonymous]
    public class EmployeesController : ApiController
    {
        private readonly IMediator mediator;
        public EmployeesController(IMediator mediator)
        {
            this.mediator = mediator;
        }
        
        // GET: api/Employees/5
        public Task<Employee.Model> Get(string id)
        {
            return mediator.SendAsync(new Employee.Query
            {
                EmployeeDisplayId = id
            });
        }

        // POST: api/Employees
        public void Post([FromBody]Create.Command command)
        {
            mediator.Send(command);
        }
    }
}
