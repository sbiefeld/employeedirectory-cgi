﻿namespace EmployeeDirectory.Api.Features.Employees
{
    using AutoMapper;
    using Core.Domain;

    public class MappingProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<Core.Domain.Employee, Employee.Model>();
            CreateMap<Address, Employee.AddressModel>();
        }
    }
}