﻿namespace EmployeeDirectory.Api.Features.Employees
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Core.Domain;
    using Core.Infrastructure;
    using FluentValidation;
    using MediatR;

    public class Create
    {
        public class Command : IRequest
        {
            public Command ()
            {
                Addresses = new List<AddressModel>();
            }

            public string FirstName { get; set; }
            public string LastName { get; set; }
            public List<AddressModel> Addresses { get; set; }
            public string EmployeeDisplayId { get; set; }
        }

        public class AddressModel
        {
            public string City { get; set; }
            public string StreetAddress { get; set; }
            public string Zip { get; set; }
            public string State { get; set; }
            public AddressTypeModel AddressType { get; set; }
        }
        public class AddressTypeModel
        {
            public Guid Id { get; set; }
            public string AddressTypeDisplayName { get; set; }
        }

        public class Validator : AbstractValidator<Command>
        {

            public Validator()
            {
                RuleFor(m => m.EmployeeDisplayId)
                    .NotEmpty()
                    .Length(1,10)
                    .Must(EmployeeDisplayIdMustBeUnique)
                    .WithMessage("Employee Display id must be unique.");

                RuleFor(m => m.FirstName).NotEmpty().Length(1,30);
                RuleFor(m => m.LastName).NotEmpty().Length(1,30);
                RuleFor(m => m.Addresses).SetCollectionValidator(new AddressValidator()).NotEmpty();
            }

            private bool EmployeeDisplayIdMustBeUnique(string employeeNumber)
            {
                bool unique;
                using (var dbContext = new EmployeeDirectoryContext())
                {
                    unique = dbContext.Employees.All(u => u.EmployeeDisplayId != employeeNumber);
                }
                return unique;
            }
        }

        public class Handler : RequestHandler<Command>
        {
            private readonly EmployeeDirectoryContext dbContext;

            public Handler(EmployeeDirectoryContext dbContext)
            {
                this.dbContext = dbContext;
            }

            protected override void HandleCore(Command message)
            {
                var employee = new Core.Domain.Employee
                {
                   
                    FirstName = message.FirstName,
                    LastName = message.LastName,
                    Active = true,
                    EmployeeDisplayId = message.EmployeeDisplayId,
                    StartDate = DateTime.UtcNow
                };
                employee.Addresses = MapAddresses(message.Addresses, employee);
                dbContext.Employees.Add(employee);
            }

            private List<Address> MapAddresses(List<AddressModel> addresses, Core.Domain.Employee employee)
            {
                return addresses.Select(m => new Address
                {
                    Active = true,
                    AddressType = dbContext.AddressTypes.FirstOrDefault(at => at.Id == m.AddressType.Id),
                    City = m.City,
                    StreetAddress = m.StreetAddress,
                    Zip = m.Zip,
                    State = m.State,
                    EmployeeId = employee.Id,

                }).ToList();
            }
        }
    }

    public class AddressValidator : AbstractValidator<Create.AddressModel>
    {
        public AddressValidator()
        {
            RuleFor(a => a.AddressType).NotEmpty();
            RuleFor(a => a.Zip).Length(5).NotEmpty();
            RuleFor(a => a.State).Length(2).NotEmpty();
            RuleFor(a => a.StreetAddress).Length(1,60).NotEmpty();
            RuleFor(a => a.City).Length(1,30).NotEmpty();
        }
    }
}