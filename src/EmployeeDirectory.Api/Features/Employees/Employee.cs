﻿namespace EmployeeDirectory.Api.Features.Employees
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using Core.Infrastructure;
    using MediatR;

    public class Employee
    {
        public class Query : IAsyncRequest<Model>
        {
            public string EmployeeDisplayId { get; set; }
        }

        public class Model
        {
            public Guid Id { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public List<AddressModel> Addresses { get; set; }
            public string EmployeeDisplayId { get; set; }
            public DateTime StartDate { get; set; }
        }

        public class QueryHandler : IAsyncRequestHandler<Query, Model>
        {
            private readonly EmployeeDirectoryContext dbContext;
            private readonly MapperConfiguration mapperConfig;

            public QueryHandler(EmployeeDirectoryContext dbContext, MapperConfiguration mapperConfig)
            {
                this.dbContext = dbContext;
                this.mapperConfig = mapperConfig;
            }

            public Task<Model> Handle(Query message)
            {
                return dbContext.Employees.Where(e => e.EmployeeDisplayId == message.EmployeeDisplayId).ProjectToFirstOrDefaultAsync<Model>(mapperConfig);
            }
        }

        public class AddressModel
        {
            public Guid Id { get; set; }
            public string AddressTypeAddressTypeDisplayName { get; set; }
            public Guid AddressTypeId { get; set; }
            public string City { get; set; }
            public string StreetAddress { get; set; }
            public string Zip { get; set; }
            public string State { get; set; }
        }
    }
}