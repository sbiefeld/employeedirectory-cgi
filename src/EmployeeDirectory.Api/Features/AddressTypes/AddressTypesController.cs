﻿namespace EmployeeDirectory.Api.Features.AddressTypes
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using System.Web.Http;
    using System.Web.Http.Cors;
    using MediatR;

    [EnableCors("*", "*", "*")]
    public class AddressTypesController : ApiController
    {
        private readonly IMediator mediator;
        public AddressTypesController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        // GET: api/AddressType
        public Task<List<Model>> Get()
        {
            return mediator.SendAsync(new Query());
        }
    }
}
