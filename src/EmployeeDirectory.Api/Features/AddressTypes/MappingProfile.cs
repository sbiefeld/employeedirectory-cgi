﻿namespace EmployeeDirectory.Api.Features.AddressTypes
{
    using AutoMapper;
    using Core.Domain;

    public class MappingProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<AddressType, Model>();
        }
    }
}