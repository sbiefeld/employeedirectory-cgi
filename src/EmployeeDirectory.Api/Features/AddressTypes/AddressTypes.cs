﻿namespace EmployeeDirectory.Api.Features.AddressTypes
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using AutoMapper;
    using Core.Infrastructure;
    using MediatR;

    public class Query : IAsyncRequest<List<Model>>
    {
    }

    public class QueryHandler : IAsyncRequestHandler<Query, List<Model>>
    {
        private readonly EmployeeDirectoryContext dbContext;
        private readonly MapperConfiguration mapperConfig;

        public QueryHandler(EmployeeDirectoryContext dbContext, MapperConfiguration mapperConfig)
        {
            this.dbContext = dbContext;
            this.mapperConfig = mapperConfig;
        }

        public Task<List<Model>> Handle(Query message)
        {
            return dbContext.AddressTypes.ProjectToListAsync<Model>(mapperConfig);
        }
    }

    public class Model
    {
        public Guid Id { get; set; }
        public string AddressTypeDisplayName { get; set; }
    }
}