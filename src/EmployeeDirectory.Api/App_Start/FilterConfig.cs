﻿namespace EmployeeDirectory.Api
{
    using System.Web.Http.Filters;
    using Infrastructure;
    using Infrastructure.Validation;

    public class FilterConfig
    {
        public static void RegisterGlobalFilters(HttpFilterCollection filters)
        {
            filters.Add(new UnitOfWork());
            filters.Add(new ValidatorActionFilter());
        }
    }
}
