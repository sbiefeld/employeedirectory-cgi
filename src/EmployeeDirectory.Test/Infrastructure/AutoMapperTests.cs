﻿namespace EmployeeDirectory.Test.Infrastructure
{
    using AutoMapper;

    public class AutoMapperTests
    {
        public void ShouldHaveValidConfiguration()
        {
            Mapper.AssertConfigurationIsValid();
        }
    }
}
