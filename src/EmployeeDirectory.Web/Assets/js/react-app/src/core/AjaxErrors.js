import _ from 'lodash';

const AjaxErrors = function () {
  if (!(this instanceof AjaxErrors)) {
    return new AjaxErrors();
  }
};

function toCamelCase(string) {
  return string[0].toLowerCase() + string.slice(1);
}

AjaxErrors.prototype.handleServerErrors = function (response) {
    let previousErrorMessages = Array.prototype.slice.call(document.querySelectorAll('.error-message'));
    if(previousErrorMessages.length) {
        previousErrorMessages.forEach((message) => {
            message.remove();
        });
    }
    let previousErrorFields = Array.prototype.slice.call(document.querySelectorAll('.input-error-highlight'));
    if(previousErrorFields.length) {
        previousErrorFields.forEach((field) => {
            field.classList.remove('input-error-highlight');
        });
    }
  Object.keys(response).forEach((key) => {
    _(response[key].Errors).forEach((error) => {
        let fieldId = toCamelCase(key),
            message = error.ErrorMessage;
        
        let messageEl = document.createElement('span');
        messageEl.className = 'error-message';
        messageEl.innerText = message;
        let field = document.querySelector('#'+fieldId);
        field.classList.add('input-error-highlight');
        field.parentElement.appendChild(messageEl);
    });
  });
};

export default AjaxErrors();
