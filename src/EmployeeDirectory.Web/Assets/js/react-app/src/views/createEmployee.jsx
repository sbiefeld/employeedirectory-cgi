﻿import React from 'react';
import ReactDOM from 'react-dom';
import JsonTable from 'react-json-table';
import Reqwest from 'reqwest';
import _ from 'lodash';
import AjaxErrors from '../core/AjaxErrors.js';

class CreateEmployee extends React.Component {
    constructor() {
        super();
        this.state = {
            employee: {}
        };
    }

    componentDidMount() {
        Reqwest({
            url: window.global_data.apiBaseUri + '/AddressTypes',
            method: 'get',
            type: 'json',
            success: (resp) => {
                var addresses = [];
                resp.forEach((type) =>{
                    addresses.push({
                        addressType: type
                    });
                });

                this.setState({
                    employee: {
                        addresses: addresses
                    }
                });

                console.debug("mounted");
                console.debug(this.state);
            }});
    }

    handleChange(field, e) {
        var employee = this.state.employee;
        employee[field] = e.target.value;
        this.setState({employee})
    }

    handleAddressChange(field, addressTypeId, e) {
        var employee = this.state.employee;
        employee.addresses.forEach((address) => {
            if(address.addressType.id === addressTypeId){
                address[field] = e.target.value;
            }            
        });

        this.setState({employee})
    }
    
    handleSave(e) {
        Reqwest({
            url: window.global_data.apiBaseUri + '/Employees/Create',
            method: 'post',
            type: 'json',
            data: this.state.employee,
            error: (xhr, msg) => {
                if (xhr.status === 400) {
                    AjaxErrors.handleServerErrors(JSON.parse(xhr.responseText));
                }
            },
            success: (resp) => {
                window.location = "/Employees";
            }
        });
    }
    renderAddressRow(address, key){
        return (
            <div key={key}>
                <div className="row">
                    <h5 className="legend">{address.addressType.addressTypeDisplayName} Address</h5>
                </div>
                <div className="row">
                    <div className="six columns">
                        <label htmlFor="streetAddress">Street Name</label>
                        <input className="u-full-width" type="text" placeholder="street name..." id="streetAddress" value={address.streetAddress} onChange={this.handleAddressChange.bind(this, 'streetAddress', address.addressType.id )} />
                    </div>
                    <div className="six columns">
                        <label htmlFor="city">City</label>
                        <input className="u-full-width" type="text" placeholder="city..." id="city" value={address.city} onChange={this.handleAddressChange.bind(this, 'city', address.addressType.id )} />
                    </div>
                </div>
                <div className="row">
                    <div className="six columns">
                        <label htmlFor="state">State</label>
                        <input className="u-full-width" type="text" placeholder="state..." id="state" value={address.state} onChange={this.handleAddressChange.bind(this, 'state', address.addressType.id )} />
                    </div>
                    <div className="six columns">
                        <label htmlFor="zip">Zip</label>
                        <input className="u-full-width" type="text" placeholder="zip..." id="zip" value={address.zip} onChange={this.handleAddressChange.bind(this, 'zip', address.addressType.id )} />
                    </div>
                </div>
            </div>
        );
    }

    render() {
        let imageUri = 'https://robohash.org/' + this.state.employee.firstName + '.' + this.state.employee.lastName + '?set=set3&size=100x100';
        let addresses = [];
        if(this.state.employee.addresses){
            addresses = this.state.employee.addresses.map((address, i) => {
                return this.renderAddressRow(address, i);
            });
        }

        return(
            <div className="container">
                <div className="section twelve columns">
                    <div className="two columns">
                        <div className="employee-photo">
                            <img src={imageUri} />
                        </div>
                    </div>
                    <div className="ten columns employee-name-title">
                        <h3 className="vertical-align">{this.state.employee.lastName}, {this.state.employee.firstName}</h3>
                    </div>
                </div>
                <div className="section twelve columns">
                    <form>
                        <div className="row">
                            <h4 className="legend">Personal Info</h4>
                        </div>
                        <div className="row">
                            <div className="six columns">
                                <label htmlFor="firstName">First Name</label>
                                <input className="u-full-width" type="text" placeholder="first name..." id="firstName" value={this.state.employee.firstName} onChange={this.handleChange.bind(this, 'firstName')} />
                            </div>
                            <div className="six columns">
                                <label htmlFor="lastName">Last Name</label>
                                <input className="u-full-width" type="text" placeholder="last name..." id="lastName" value={this.state.employee.lastName} onChange={this.handleChange.bind(this, 'lastName')} />
                            </div>
                        </div>
                        <div className="row">
                            <div className="six columns">
                                <label htmlFor="employeeDisplayId">Employee Id</label>
                                <input className="u-full-width" type="text" placeholder="0000000000" id="employeeDisplayId" value={this.state.employee.employeeDisplayId} onChange={this.handleChange.bind(this, 'employeeDisplayId')} />
                            </div>
                            <div className="six columns">
                                <label htmlFor="startDate">Start Date</label>
                                <input className="u-full-width" type="date" placeholder="0000000000" id="startDate" value={this.state.employee.startDate} onChange={this.handleChange.bind(this, 'startDate')} />
                            </div>
                        </div>
                        {addresses}
                    </form>
                </div>
                <div className="twelve columns section">
                    <div className="row">
                        <div className="six columns">
                            <a className="button u-full-width" href="/Employees">Back</a>
                        </div>
                        <div className="six columns">
                            <button className="button-primary u-full-width" onClick={this.handleSave.bind(this)} >Save</button>
                        </div>
                    </div>
                </div>
            </div>
        );
            }
}
const componentDom = document.getElementById('react-create-employee');

if (componentDom) {
    ReactDOM.render(<CreateEmployee />, componentDom);
}

export default CreateEmployee;
