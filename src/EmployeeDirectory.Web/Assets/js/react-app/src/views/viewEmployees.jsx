﻿import React from 'react';
import ReactDOM from 'react-dom';
import JsonTable from 'react-json-table';
import Reqwest from 'reqwest';
import moment from 'moment';

class ViewEmployees extends React.Component {
    constructor() {
        super();
        this.state = {
            query: '',
            employee: {},
            columns: [
                {key: 'addressTypeAddressTypeDisplayName', label: 'Type'},
                {key: 'streetAddress', label: 'Street'},
                {key: 'city', label: 'City'},
                {key: 'zip', label: 'Zip'}
            ],
            tableSettings: { 
                keyField: 'id',
                noRowsMessage: 'This employee does not have any addresses on file.'
            }
        };
    }
    toggleResults(show){
        let noResultsContainer = document.querySelector('#no-results');
        let resultsContainer = document.querySelector('#results');

        if(show){
            noResultsContainer.style.display = 'none';
            resultsContainer.style.display = 'block';
        } else {
            noResultsContainer.style.display = 'block';
            resultsContainer.style.display = 'none';
        }
    }
    handleChange(field, e) {
        var query = e.currentTarget.value;
        if(query){
            this.findEmployee.call(this, query);   
        } else {
            this.toggleResults(false);
        }
    }
    findEmployee(query){        
        Reqwest({
            url: window.global_data.apiBaseUri + '/Employees/' + query,
            method: 'GET',
            type: 'json',
            data: {},
            error: (error) => {
                console.debug(error);
            },
            success: (response) => {
                
                var updatedState = this.state;
                if(response){
                    updatedState.employee = response;
                    this.toggleResults(true);
                } else {
                    updatedState.employee = {};
                    this.toggleResults(false);
                }
                this.setState(updatedState);
            }
        });
    }
    render() {
        //let formattedDate = Moment(this.state.employee.startDate, 'MM-DD-YY');
        return(<div className="twelve columns">
            <h2>Employee Search</h2>
            <input type="search" className="u-full-width" id="search" placeholder="Search by Employee Id..." onChange={this.handleChange.bind(this, 'query')} />
            <div className="section twelve columns" id="no-results">We couldn't find an employee with that Employee Id, please try another.</div>
            <div style={{ display:'none' }} id="results" className="section twelve columns">
                <div className="row">
                    <h4 className="legend">Personal Info</h4>
                </div>
                <div className="row">
                    <div className="three columns">
                        <label htmlFor="firstName">First Name</label>
                        <p className="u-full-width" id="firstName" >{ this.state.employee.firstName }</p>
                    </div>
                    <div className="three columns">
                        <label htmlFor="lastName">Last Name</label>
                        <p className="u-full-width" id="lastName" >{ this.state.employee.lastName }</p>
                    </div>
                    <div className="three columns">
                        <label htmlFor="displayId">Employee Id</label>
                        <p className="u-full-width" id="displayId" >{ this.state.employee.employeeDisplayId }</p>
                    </div>
                    <div className="three columns">
                        <label htmlFor="startDate">Start Date</label>
                        <p className="u-full-width" id="startDate" > {moment.utc(this.state.employee.startDate).format('MMM D, YYYY')}</p>
                    </div>
                </div>
                <div className="row">
                    <h4 className="legend">Addresses</h4>
                </div>
                <div className="row">
                    <JsonTable rows={ this.state.employee.addresses } columns={ this.state.columns } settings={ this.state.tableSettings } />
                </div>
            </div>
            <a className="button u-full-width" href="/Employees/Create">Create Employee</a>
        </div>);
            }
}
const componentDom = document.getElementById('react-view-employees');

    if (componentDom) {
        ReactDOM.render(<ViewEmployees/>, componentDom);
    }

export default ViewEmployees;
