# react-app

## Prerequisites

You will need the following things properly installed on your computer.

* [Git](http://git-scm.com/)
* [Node.js](http://nodejs.org/) (with npm)
* [nwb](https://github.com/insin/nwb/) - `npm install -g nwb`

## Installation

* `git clone <repository-url>` this repository
* change into the new directory
* `npm install`

## Running / Development

* `nwb serve` will run the react app; do this before debugging the VS solution

### Running Tests

* `nwb test` will run the tests once
* `nwb test --server` will run the tests on every change

### Building

You do not need to run these locally; the build script will run it as part of the CI

* `nwb build` (production)
* `nwb build --set-env-NODE_ENV=development` (development)
