﻿namespace EmployeeDirectory.Features
{
    using System.Configuration;
    using System.Web.Mvc;

    public static class HtmlExtensions
    {
        public static bool IsDebug(this HtmlHelper htmlHelper)
        {
            #if DEBUG
                return true;
            #else
                return false;
            #endif
        }

        public static string ApiBaseUri(this HtmlHelper htmlHelper)
        {
            return ConfigurationManager.AppSettings.Get("APIBaseURI");
        }
    }
}