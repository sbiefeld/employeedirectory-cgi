﻿namespace EmployeeDirectory.Features.Employees
{
    using System.Threading.Tasks;
    using System.Web.Mvc;

    public class EmployeesController : Controller
    {
        public async Task<ActionResult> Index()
        {
            return View();
        }

        public ActionResult Create()
        {
            return View();
        }
    }
}