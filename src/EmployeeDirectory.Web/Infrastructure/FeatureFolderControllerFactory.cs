﻿namespace EmployeeDirectory.Infrastructure
{
    using System;
    using System.Web.Mvc;
    using System.Web.Routing;

    public class FeatureFolderControllerFactory : DefaultControllerFactory
    {
        protected override Type GetControllerType(RequestContext requestContext, string controllerName)
        {
            string typeName = $"EmployeeDirectory.Features.{controllerName}.{controllerName}Controller";

            return typeof(FeatureFolderControllerFactory).Assembly.GetType(typeName);
        }
    }
}