﻿namespace EmployeeDirectory
{
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Optimization;
    using System.Web.Routing;
    using Infrastructure;

    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            EnableFeatureFolders();
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        private static void EnableFeatureFolders()
        {
            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new FeatureFolderRazorViewEngine());
            ControllerBuilder.Current.SetControllerFactory(new FeatureFolderControllerFactory());
        }
    }
}
